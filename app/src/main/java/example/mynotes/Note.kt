package example.mynotes

class Note{
    var noteID:Int? = null
    var noteTitle:String? = null
    var noteValue:String? = null

    constructor(noteId:Int, noteTitle:String, noteValue:String){
        this.noteID = noteId
        this.noteTitle = noteTitle
        this.noteValue = noteValue
    }
}
