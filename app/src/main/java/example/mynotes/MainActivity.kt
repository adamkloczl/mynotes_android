package example.mynotes

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SearchView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.listview_schema.view.*

class MainActivity : AppCompatActivity() {

    var listNotes = ArrayList<Note>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Load from database
        loadQuery("%")


    }

    fun loadQuery(title:String){
        var db = DbManager(this)
        var projection = arrayOf("ID", "Title", "Description")
        var selectionArgs = arrayOf(title)
        var cursor = db.query(projection, "Title like ?", selectionArgs, "Title")
        listNotes.clear()
        if(cursor.moveToFirst()){
            do {
                var ID = cursor.getInt(cursor.getColumnIndex("ID"))
                var title = cursor.getString(cursor.getColumnIndex("Title"))
                var description = cursor.getString(cursor.getColumnIndex("Description"))

                listNotes.add(Note(ID, title, description))
            }while (cursor.moveToNext())
        }
        var myNoteAdapter = MyNotesAdapter(this,listNotes)
        noteListView.adapter = myNoteAdapter
    }

    override fun onResume() {
        super.onResume()
        loadQuery("%")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)

        var searchView = menu!!.findItem(R.id.app_bar_search).actionView as SearchView
        var searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                loadQuery("%$query%")
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                loadQuery("%$query%")
                return  false
            }

        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when(item.itemId){
                R.id.newNote -> {
                    var intent = Intent(this, AddNotes::class.java)
                    startActivity(intent)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }



    inner  class MyNotesAdapter:BaseAdapter{
        var context:Context? = null
        var listNotesAdapter = ArrayList<Note>()
        constructor(context:Context,listNotes:ArrayList<Note>):super(){
            this.listNotesAdapter = listNotes
            this.context = context
        }
        override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

            var myView = layoutInflater.inflate(R.layout.listview_schema,null)
            var myNote = listNotesAdapter[p0]

            myView.titleBU.text = myNote.noteTitle
            myView.descriptionBu.text = myNote.noteValue
            myView.deleteBu.setOnClickListener({
                var dbManager = DbManager(context!!)
                var selectionArgs = arrayOf(myNote.noteID.toString())
                dbManager.delete("ID = ?", selectionArgs)
                loadQuery("%")
            })
            myView.editBu.setOnClickListener({
                goToUpdate(myNote, myView)
            })

            return myView
        }

        override fun getItem(p0: Int): Any {
            return listNotesAdapter[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getCount(): Int {
            return listNotesAdapter.size
        }
    }

    fun goToUpdate(note:Note, view: View){
        var intent = Intent(this, AddNotes::class.java)
        intent.putExtra("ID", note.noteID)
        intent.putExtra("name", note.noteTitle)
        intent.putExtra("des", note.noteValue)
        startActivity(intent)
    }
}
