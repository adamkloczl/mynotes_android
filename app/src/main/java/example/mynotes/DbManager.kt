package example.mynotes

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder

class DbManager {
    val dbName = "MyNotes"
    val dbTable ="Notes"
    val colId = "ID"
    val colTitle = "Title"
    val colDescription = "Description"
    val dbVersion = 1
    val sqlCreateTable = "CREATE TABLE IF NOT EXISTS $dbTable ($colId INTEGER PRIMARY KEY, "+
            "$colTitle TEXT, $colDescription TEXT);"
    var sqlDB:SQLiteDatabase? = null

    constructor(context:Context){
        val db = DataBaseHelperNotes(context)
        sqlDB = db.writableDatabase
    }

    inner class DataBaseHelperNotes:SQLiteOpenHelper{
        var context:Context? = null
        constructor(context: Context):super(context, dbName, null,dbVersion){
            this.context = context
        }
        override fun onCreate(db: SQLiteDatabase?) {
            db!!.execSQL(sqlCreateTable)
            //Toast.makeText(context, "database is created", Toast.LENGTH_SHORT).show()
        }

        override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
            db!!.execSQL("Drop table IF EXISTS $dbTable")
        }

    }

    fun insert(value:ContentValues):Long{
        return sqlDB!!.insert(dbTable, "", value)
    }

    fun query(projection:Array<String>, selection:String, selectionArgs: Array<String>, sortOrder:String): Cursor {
        var qb = SQLiteQueryBuilder()
        qb.tables = dbTable
        return qb.query(sqlDB, projection, selection,selectionArgs,null,null,sortOrder)
    }

    fun delete(selection:String, selectionArgs:Array<String>):Int{
        return sqlDB!!.delete(dbTable, selection,selectionArgs)
    }

    fun update(values:ContentValues, selection:String,selectionArgs: Array<String>):Int{
        return sqlDB!!.update(dbTable, values, selection,selectionArgs)
    }

}