package example.mynotes

import android.content.ContentValues
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_notes.*

class AddNotes : AppCompatActivity() {

    var id=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_notes)

        try {
            var bundle:Bundle = intent.extras
            id=bundle.getInt("ID",0)
            if(id!= 0){
                noteTitle.setText(bundle.getString("name").toString())
                noteValue.setText(bundle.getString("des").toString())
            }
        }catch (e:Exception){}
    }

    fun buAdd(view: View){
        var dbManager = DbManager(this)
        var values = ContentValues()
        values.put("Title", noteTitle.text.toString())
        values.put("Description", noteValue.text.toString())

        if(id == 0){
            val ID = dbManager.insert(values)
            if(ID > 0){
                Toast.makeText(this, "note is added", Toast.LENGTH_SHORT).show()
                finish()
            }else{
                Toast.makeText(this, "can not add note", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
        else{
            var selectionArgs = arrayOf(id.toString())
            var ID = dbManager.update(values, "ID = ?", selectionArgs)
            if(ID > 0){
                Toast.makeText(this, "note is added", Toast.LENGTH_SHORT).show()
                finish()
            }else{
                Toast.makeText(this, "can not add note", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}
